require 'asciidoctor'
require 'asciidoctor/extensions'

class LuaInterpretationBlockMacro < Asciidoctor::Extensions::BlockMacroProcessor
  use_dsl

  named :luainter

  def process parent, target, attrs
    title_html = (attrs.has_key? 'title') ?
        %(<div class="title">#{attrs['title']}</div>\n) : nil
    role = " #{attrs['role']}" if attrs.has_key? 'role'

    html = %(<div class="openblock videoblock#{role}">
#{title_html}<div class="content">
<video controls preload="metadata">
<source src="#{target}" type="video/mp4">
</video>
<ul class="controls">
<li>
<button type="button"
        onclick="event.target.parentNode.parentNode.parentNode.children[0].currentTime = Math.max(event.target.parentNode.parentNode.parentNode.children[0].currentTime - 1/4, 0)">
← 1 кадр туда
</button>
</li>
<li class="rate-slider">
<div>4 FPS</div>
<input type="range"
       min="1"
       max="20"
       step="1"
       value="4"
       autocomplete="off"
       onchange="event.target.parentNode.parentNode.parentNode.children[0].playbackRate = event.target.value / 4; event.target.parentNode.children[0].innerText = event.target.value + ' FPS'">
</li>
<li>
<button type="button"
        onclick="event.target.parentNode.parentNode.parentNode.children[0].currentTime = Math.min(event.target.parentNode.parentNode.parentNode.children[0].currentTime + 1/4, event.target.parentNode.parentNode.parentNode.children[0].duration)">
1 кадр туда →
</button>
</li>
</ul>
</div>
</div>)

    create_pass_block parent, html, attrs, subs: nil
  end
end

Asciidoctor::Extensions::register do
  block_macro LuaInterpretationBlockMacro if document.basebackend? 'html'
end
